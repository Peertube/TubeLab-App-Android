## TubeLab/TubeAcad [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

This project groups two different apps. **[TubeAcad](#TubeAcad)**, a Peertube Android app for French academic authorities. All is in French. Its use is limited to some instances.
The other app is **[TubeLab](#TubeLab)** a Peertube Android app working for all instances.

 
## <a name="TubeLab">TubeLab</a>

Tubelab is an Android app for Peertube (GNU GPLv3). <img src='https://img.shields.io/f-droid/v/app.fedilab.tubelab?include_prereleases' />

[<img alt='Get it on Google Play' src='./images/get-it-on-play.png' height="80"/>](https://play.google.com/store/apps/details?id=app.fedilab.tubelab)
&nbsp;&nbsp;[<img alt='Get it on F-Droid' src='./images/get-it-on-fdroid.png' height="80"/>](https://f-droid.org/packages/app.fedilab.tubelab/)

### Not authenticated mode

It's a limited mode where you can do some actions:

- Switch instance
- Share videos
- Download videos

### Authenticated mode

Many features are available with this mode:

- Write/delete comments
- Upload/remove/edit videos
- Manage (create/edit/remove) channels and playlists
- Follow/unfollow channels
- Thumbs-up/down
- Check notifications
- Mute/unmute channels
- Report videos/accounts
- Check your history

## <a name="TubeAcad">TubeAcad</a>

TubeAcad est une application Android open source (GNU GPLv3) pour les instances Peertube académiques. L’authentification se fait par adresse mail, l’instance est automatiquement détectée. Il est également possible sur certaines instances de créer son compte depuis l’application.


[<img alt='Get it on Google Play' src='./images/get-it-on-play.png' height="80"/>](https://play.google.com/store/apps/details?id=app.fedilab.fedilabtube)
&nbsp;&nbsp;[<img alt='Get it on F-Droid' src='./images/get-it-on-fdroid.png' height="80"/>](https://f-droid.org/packages/app.fedilab.fedilabtube/)


### Mode non authentifié

Ce mode permet de visionner les vidéos sur différentes instances (en sélectionnant une instance académique). Cependant, vous ne pourrez pas interagir totalement avec les vidéos. Vous pourrez :

- Partager une vidéo,
- Télécharger une vidéo.


### Mode authentifié

Si vous connectez votre compte, vous pourrez interagir avec les vidéos :

- Écrire/supprimer un commentaire,
- Ajouter/supprimer des vidéos dans les listes de lecture,
- Créer/Supprimer des listes de lecture,
- Suivre une chaîne,
- Thumbs-up/down,
- Téléverser des vidéos,
- Modifier vos vidéos,
- Supprimer vos vidéos,
- Voir vos notifications.
- Créer/supprimer une chaîne
- Mettre des comptes en sourdine
- Signaler des vidéos ou des comptes
- Voir l'historique

