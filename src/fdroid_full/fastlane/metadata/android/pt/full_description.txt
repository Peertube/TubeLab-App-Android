*Não autenticado*

É um modo limitado onde você pode realizar algumas ações:

- Trocar de instância,
- Compartilhar vídeos,
- Download de vídeos.


*Modo autenticado*

Muitas características estão disponíveis com este modo:

- Escrever/apagar comentários
- Upload/remoção/edição de vídeos
- Gerenciar (criar/editar/remover) canais e listas de reprodução
- Seguir/sempreender os canais
- Polegares para cima/baixo
- Verificar notificações
- Canais de mudo/unmudo
- Relatar vídeos/contas
- Confira seu histórico