*Not authenticated mode*

It's a limited mode where you can do some actions:

- Switch instance,
- Share videos,
- Download videos.


*Authenticated mode*

Many features are available with this mode:

- Write/delete comments
- Upload/remove/edit videos
- Manage (create/edit/remove) channels and playlists
- Follow/unfollow channels
- Thumbs-up/down
- Check notifications
- Mute/unmute channels
- Report videos/accounts
- Check your history