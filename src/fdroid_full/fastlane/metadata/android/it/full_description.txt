*Modalità non autenticata*

È una modalità limitata in cui è possibile fare alcune azioni:

- Cambia istanza,
- Condivi video,
- Scarica video.


*Modalità autenticata*

Molte funzioni sono disponibili con questa modalità:

- Scrittura/elimina commenti
- Carica/rimuovi/modifica video
- Gestisci (crea/modifica/rimuovi) i canali e le playlist
- Seguire/non seguire i canali
- Pollici su/giù
- Controlla le notifiche
- Disattivare/disattivare i canali
- Segnala video/account
- Controlla la tua cronologia