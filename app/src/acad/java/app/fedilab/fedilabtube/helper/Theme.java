package app.fedilab.fedilabtube.helper;
/* Copyright 2022 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */
import androidx.appcompat.app.AppCompatActivity;

import app.fedilab.fedilabtube.R;

public class Theme {

    public static void setTheme(AppCompatActivity activity, String instance, boolean noActionBar) {

        switch (instance) {
            case "tube-institutionnel.apps.education.fr":
                activity.setTheme(noActionBar? R.style.InstitutionnelNoActionBar:R.style.Institutionnel);
                break;
            case "tube-maternelle.apps.education.fr":
                activity.setTheme(noActionBar?R.style.MaternelleNoActionBar:R.style.Maternelle);
                break;
            case "tube-arts-lettres-sciences-humaines.apps.education.fr":
                activity.setTheme(noActionBar?R.style.ArtNoActionBar:R.style.Art);
                break;
            case "tube-sciences-technologies.apps.education.fr":
                activity.setTheme(noActionBar?R.style.SciencesNoActionBar:R.style.Sciences);
                break;
            case "tube-education-physique-et-sportive.apps.education.fr":
                activity.setTheme(noActionBar?R.style.EducationNoActionBar:R.style.Education);
                break;
            case "tube-enseignement-professionnel.apps.education.fr":
                activity.setTheme(noActionBar?R.style.EnseignementProNoActionBar:R.style.EnseignementPro);
                break;
            case "tube-langues-vivantes.apps.education.fr":
                activity.setTheme(noActionBar?R.style.LanguesNoActionBar:R.style.Langues);
                break;
            case "tube-action-educative.apps.education.fr":
                activity.setTheme(noActionBar?R.style.ActionEducativeNoActionBar:R.style.ActionEducative);
                break;
            case "tube-cycle-2.apps.education.fr":
                activity.setTheme(noActionBar?R.style.Cycle2NoActionBar:R.style.Cycle2);
                break;
            case "tube-cycle-3.apps.education.fr":
                activity.setTheme(noActionBar?R.style.Cycle3NoActionBar:R.style.Cycle3);
                break;
            default:
                activity.setTheme(noActionBar?R.style.AppThemeNoActionBar:R.style.AppTheme);
        }
    }
}
