<?xml version="1.0" encoding="utf-8"?><!--
    Copyright 2020 Thomas Schneider

    This file is a part of TubeLab

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU General Public License as published by the Free Software Foundation; either version 3 of the
    License, or (at your option) any later version.

    TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
    Public License for more details.

    You should have received a copy of the GNU General Public License along with TubeLab; if not,
    see <http://www.gnu.org/licenses>.
-->
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:keepScreenOn="true"
    tools:context=".activities.PeertubeActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        android:orientation="vertical">

        <RelativeLayout
            android:id="@+id/video_container"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:layout_weight="1">

            <FrameLayout
                android:id="@+id/main_media_frame"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:layout_centerInParent="true"
                android:layout_gravity="center"
                android:background="#000000"
                android:animateLayoutChanges="true"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintLeft_toLeftOf="parent"
                app:layout_constraintRight_toRightOf="parent"
                app:layout_constraintTop_toTopOf="parent">
                <com.github.vkay94.dtpv.DoubleTapPlayerView
                    android:id="@+id/doubleTapPlayerView"
                    android:animateLayoutChanges="true"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    app:use_controller="true"
                    app:dtpv_controller="@+id/media_video" />

                <com.github.vkay94.dtpv.youtube.YouTubeOverlay
                    android:id="@+id/media_video"
                    android:animateLayoutChanges="true"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:visibility="invisible"
                    app:yt_playerView="@+id/doubleTapPlayerView"
                    android:gravity="center" />

                <ImageView
                    android:id="@+id/watermark"
                    android:layout_width="100dp"
                    android:layout_height="40dp"
                    android:layout_gravity="top|start"
                    android:contentDescription="@string/watermark"
                    android:scaleType="fitCenter"
                    android:visibility="gone" />

                <include
                    android:id="@+id/min_controller"
                    layout="@layout/min_controller" />

                <app.fedilab.fedilabtube.webview.CustomWebview
                    android:id="@+id/webview_video"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:visibility="gone" />

            </FrameLayout>

            <!-- Main Loader -->
            <RelativeLayout
                android:id="@+id/loader"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:gravity="center"
                android:visibility="gone">

                <com.github.ybq.android.spinkit.SpinKitView
                    style="@style/SpinKitView.DoubleBounce"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center" />
            </RelativeLayout>
        </RelativeLayout>

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="match_parent"
            android:id="@+id/bottom_video"
            android:layout_height="0dp"
            android:layout_weight="2"
            android:layout_marginStart="@dimen/video_comment_margin"
            android:layout_marginEnd="@dimen/video_comment_margin"
            >
            <androidx.core.widget.NestedScrollView
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintBottom_toBottomOf="parent"
                android:id="@+id/peertube_information_container"
                android:layout_width="match_parent"
                android:layout_height="match_parent">
                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="vertical">

                    <androidx.cardview.widget.CardView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content">

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:orientation="vertical"
                            android:padding="10dp">

                            <LinearLayout
                                android:layout_width="match_parent"
                                android:layout_height="wrap_content"
                                android:orientation="horizontal"
                                tools:ignore="UseCompoundDrawables">

                                <TextView
                                    android:id="@+id/peertube_title"
                                    android:layout_width="0dp"
                                    android:layout_height="wrap_content"
                                    android:layout_marginTop="5dp"
                                    android:layout_weight="1" />

                                <ImageView
                                    android:id="@+id/video_sensitive"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:contentDescription="@string/sensitive_content"
                                    android:gravity="center_horizontal"
                                    android:src="@drawable/ic_sensitive_content"
                                    android:visibility="invisible" />
                            </LinearLayout>

                            <LinearLayout
                                android:layout_width="match_parent"
                                android:layout_height="wrap_content"
                                android:layout_marginTop="5dp"
                                android:orientation="horizontal">

                                <ImageView
                                    android:id="@+id/pp_channel"
                                    android:layout_width="50dp"
                                    android:layout_height="50dp"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:contentDescription="@string/profile_picture" />

                                <TextView
                                    android:id="@+id/peertube_view_count"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:drawablePadding="5dp"
                                    android:gravity="center_horizontal"
                                    android:text="0"
                                    app:drawableTopCompat="@drawable/ic_baseline_visibility_24"
                                    tools:ignore="HardcodedText" />

                                <TextView
                                    android:id="@+id/peertube_like_count"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="20dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:drawablePadding="5dp"
                                    android:gravity="center_horizontal"
                                    android:text="0"
                                    app:drawableTopCompat="@drawable/ic_baseline_thumb_up_alt_24"
                                    tools:ignore="HardcodedText" />

                                <TextView
                                    android:id="@+id/peertube_dislike_count"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:drawablePadding="5dp"
                                    android:gravity="center_horizontal"
                                    android:text="0"
                                    app:drawableTopCompat="@drawable/ic_baseline_thumb_down_alt_24"
                                    tools:ignore="HardcodedText" />

                                <TextView
                                    android:id="@+id/peertube_reblog"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:drawablePadding="5dp"
                                    android:gravity="center_horizontal"
                                    android:text="0"
                                    android:visibility="gone"
                                    app:drawableTopCompat="@drawable/ic_baseline_repeat_24"
                                    tools:ignore="HardcodedText" />

                                <TextView
                                    android:id="@+id/peertube_favorite"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:drawablePadding="5dp"
                                    android:gravity="center_horizontal"
                                    android:text="0"
                                    android:visibility="gone"
                                    app:drawableTopCompat="@drawable/ic_baseline_star_24"
                                    tools:ignore="HardcodedText" />

                                <TextView
                                    android:id="@+id/peertube_bookmark"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:drawablePadding="5dp"
                                    android:gravity="center_horizontal"
                                    android:text=""
                                    android:visibility="gone"
                                    app:drawableTopCompat="@drawable/ic_baseline_bookmark_24"
                                    tools:ignore="HardcodedText" />


                                <ImageView
                                    android:id="@+id/peertube_playlist"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:drawablePadding="5dp"
                                    android:gravity="center_horizontal"
                                    android:contentDescription="@string/playlists"
                                    android:visibility="gone"
                                    android:src="@drawable/ic_baseline_list_24" />

                                <ImageView
                                    android:id="@+id/video_information"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:gravity="center_horizontal"
                                    android:contentDescription="@string/information"
                                    android:src="@drawable/ic_baseline_info_24" />


                                <LinearLayout
                                    android:layout_width="0dp"
                                    android:layout_height="wrap_content"
                                    android:layout_weight="1"
                                    tools:ignore="UselessLeaf" />


                                <TextView
                                    android:id="@+id/more_actions"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_gravity="center_horizontal"
                                    android:layout_marginStart="10dp"
                                    android:layout_marginLeft="10dp"
                                    android:layout_marginEnd="10dp"
                                    android:layout_marginRight="10dp"
                                    android:drawablePadding="5dp"
                                    android:gravity="center_horizontal"
                                    android:text=""
                                    app:drawableTopCompat="@drawable/ic_baseline_more_vert_24" />

                            </LinearLayout>

                            <TextView
                                android:id="@+id/peertube_description"
                                android:layout_width="match_parent"
                                android:layout_height="wrap_content"
                                android:layout_marginTop="10dp"
                                android:autoLink="web"
                                android:textIsSelectable="true" />

                            <TextView
                                android:id="@+id/peertube_description_more"
                                android:layout_width="match_parent"
                                android:layout_height="wrap_content"
                                android:layout_marginTop="2dp"
                                android:text="@string/show_more"
                                android:textColor="?attr/colorAccent"
                                android:visibility="gone" />
                        </LinearLayout>
                    </androidx.cardview.widget.CardView>

                    <Button
                        android:layout_gravity="center"
                        style="@style/Widget.AppCompat.Button.Borderless.Colored"
                        android:id="@+id/post_comment_button"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@string/add_public_comment"/>
                    <androidx.recyclerview.widget.RecyclerView
                        android:layout_marginStart="10dp"
                        android:id="@+id/peertube_comments"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />

                    <RelativeLayout
                        android:id="@+id/no_action"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:visibility="gone">

                        <TextView
                            android:id="@+id/no_action_text"
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:layout_gravity="center"
                            android:gravity="center"
                            android:padding="10dp"
                            android:text="@string/no_comments"
                            android:textSize="25sp" />
                    </RelativeLayout>
                </LinearLayout>
            </androidx.core.widget.NestedScrollView>

            <androidx.core.widget.NestedScrollView
                android:background="?android:colorBackground"
                android:visibility="gone"
                app:layout_constraintTop_toTopOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintBottom_toBottomOf="parent"
                android:id="@+id/reply_thread"
                android:layout_width="match_parent"
                android:layout_height="match_parent">
                <LinearLayout
                    android:id="@+id/main_container"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:divider="?android:dividerHorizontal"
                    android:orientation="vertical"
                    android:showDividers="end">
                    <ImageView
                        android:layout_gravity="end|center_vertical"
                        android:id="@+id/close_reply"
                        android:layout_width="24dp"
                        android:layout_height="24dp"
                        android:contentDescription="@string/close"
                        android:src="@drawable/ic_baseline_close_24" />

                    <androidx.recyclerview.widget.RecyclerView
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        android:id="@+id/peertube_reply"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content" />
                </LinearLayout>

            </androidx.core.widget.NestedScrollView>


        </androidx.constraintlayout.widget.ConstraintLayout>

        <!-- View where the video will be shown when video goes fullscreen -->
        <RelativeLayout
            android:id="@+id/videoLayout"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="@android:color/black"
            android:visibility="gone" />
    </LinearLayout>

    <RelativeLayout
        android:id="@+id/video_params"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="?attr/backgroundView"
        android:minHeight="100dp"
        android:translationZ="2dp"
        android:visibility="gone"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent">

        <androidx.recyclerview.widget.RecyclerView
            android:id="@+id/main_options_video"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center"
            android:layout_margin="30dp" />
    </RelativeLayout>

    <androidx.core.widget.NestedScrollView
        android:id="@+id/post_comment"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="?android:colorBackground"
        android:visibility="gone"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent">

        <LinearLayout
            android:id="@+id/post_main_container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_margin="10dp"
            android:divider="?android:dividerHorizontal"
            android:orientation="vertical"
            android:showDividers="end">

            <ImageView
                android:id="@+id/close_post"
                android:layout_width="24dp"
                android:layout_height="24dp"
                android:layout_gravity="end|center_vertical"
                android:layout_margin="5dp"
                android:contentDescription="@string/close"
                android:src="@drawable/ic_baseline_close_24" />

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/reply_content"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:visibility="gone">

                <ImageView
                    android:id="@+id/comment_account_profile"
                    android:layout_width="40dp"
                    android:layout_height="40dp"
                    android:layout_marginTop="5dp"
                    android:contentDescription="@string/profile_picture"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <TextView
                    android:id="@+id/comment_account_displayname"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="5dp"
                    android:singleLine="true"
                    android:textStyle="bold"
                    app:layout_constraintEnd_toStartOf="@+id/comment_date"
                    app:layout_constraintStart_toEndOf="@+id/comment_account_profile"
                    app:layout_constraintTop_toTopOf="parent" />

                <TextView
                    android:id="@+id/comment_account_username"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="5dp"
                    android:layout_marginTop="2dp"
                    android:ellipsize="end"
                    android:singleLine="true"
                    android:textSize="12sp"
                    app:layout_constraintStart_toEndOf="@+id/comment_account_profile"
                    app:layout_constraintTop_toBottomOf="@+id/comment_account_displayname" />

                <TextView
                    android:id="@+id/comment_date"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="end"
                    android:layout_marginStart="2dp"
                    android:layout_weight="0"
                    android:gravity="end"
                    android:maxLines="1"
                    android:textSize="12sp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <TextView
                    android:id="@+id/comment_content"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="5dp"
                    android:textIsSelectable="true"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/comment_account_profile" />
            </androidx.constraintlayout.widget.ConstraintLayout>

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/write_comment_container"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="10dp">

                <View
                    android:id="@+id/separator_top"
                    android:layout_width="match_parent"
                    android:layout_height="1px"
                    android:layout_margin="5dp"
                    android:background="@android:color/darker_gray"
                    app:layout_constraintBottom_toTopOf="@+id/write_container"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:id="@+id/write_container"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    app:layout_constraintBottom_toTopOf="@+id/separator_bottom"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/separator_top">

                    <ImageView
                        android:id="@+id/my_pp"
                        android:layout_width="30dp"
                        android:layout_height="30dp"
                        android:contentDescription="@string/profile_picture"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent" />

                    <com.google.android.material.textfield.TextInputLayout
                        android:id="@+id/text_field_boxes"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toEndOf="@+id/my_pp"
                        app:layout_constraintTop_toTopOf="parent">

                        <com.google.android.material.textfield.TextInputEditText
                            android:id="@+id/add_comment_write"
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:gravity="top"
                            android:hint="@string/add_public_reply"
                            android:inputType="textMultiLine"
                            android:maxLines="8"
                            android:minLines="4" />
                    </com.google.android.material.textfield.TextInputLayout>

                    <Button
                        android:id="@+id/send"
                        style="@style/Widget.AppCompat.Button.Borderless.Colored"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:text="@string/send_comment"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintTop_toBottomOf="@+id/text_field_boxes" />

                </androidx.constraintlayout.widget.ConstraintLayout>

                <View
                    android:id="@+id/separator_bottom"
                    android:layout_width="match_parent"
                    android:layout_height="1px"
                    android:layout_margin="5dp"
                    android:background="@android:color/darker_gray"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/write_container" />
            </androidx.constraintlayout.widget.ConstraintLayout>
        </LinearLayout>

    </androidx.core.widget.NestedScrollView>
    <androidx.core.widget.NestedScrollView
        android:id="@+id/video_params_submenu"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="?attr/backgroundView"
        android:minHeight="300dp"
        android:translationZ="3dp"
        android:visibility="gone"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent">

        <RelativeLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content">

            <TextView
                android:id="@+id/subMenuTitle"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/fab_margin"
                android:textSize="16sp" />

            <androidx.recyclerview.widget.RecyclerView
                android:id="@+id/subMenuRecycler"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_gravity="center"
                android:layout_margin="30dp" />
        </RelativeLayout>
    </androidx.core.widget.NestedScrollView>
</androidx.constraintlayout.widget.ConstraintLayout>
