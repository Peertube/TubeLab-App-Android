<?xml version="1.0" encoding="utf-8"?><!--
    Copyright 2020 Thomas Schneider

    This file is a part of TubeLab

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU General Public License as published by the Free Software Foundation; either version 3 of the
    License, or (at your option) any later version.

    TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
    Public License for more details.

    You should have received a copy of the GNU General Public License along with TubeLab; if not,
    see <http://www.gnu.org/licenses>.
-->
<androidx.coordinatorlayout.widget.CoordinatorLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:fitsSystemWindows="true"
    tools:context=".activities.AccountActivity">

    <com.google.android.material.appbar.AppBarLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:fitsSystemWindows="true"
        app:theme="@style/ThemeOverlay.AppCompat.ActionBar">

        <com.google.android.material.appbar.CollapsingToolbarLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:fitsSystemWindows="true"
            app:contentScrim="?attr/colorPrimary"
            app:expandedTitleMarginEnd="64dp"
            app:expandedTitleMarginStart="48dp"
            app:layout_scrollFlags="scroll|exitUntilCollapsed">

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <TextView
                    android:id="@+id/instance"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="5dp"
                    android:gravity="center"
                    android:textColor="@android:color/white"
                    android:textSize="18sp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent" />

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:padding="10dp">

                    <ImageView
                        android:id="@+id/profile_picture"
                        android:layout_width="72dp"
                        android:layout_height="72dp"
                        android:contentDescription="@string/profile_picture"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent" />

                    <TextView
                        android:id="@+id/displayname"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="20dp"
                        android:ellipsize="end"
                        android:singleLine="true"
                        android:textColor="@android:color/white"
                        android:textSize="18sp"
                        app:layout_constraintRight_toLeftOf="@+id/button_container"
                        app:layout_constraintStart_toEndOf="@+id/profile_picture"
                        app:layout_constraintTop_toTopOf="@+id/profile_picture" />

                    <TextView
                        android:id="@+id/username"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="20dp"
                        android:layout_marginTop="30dp"
                        android:ellipsize="end"
                        android:singleLine="true"
                        android:textColor="@android:color/white"
                        android:textSize="14sp"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintRight_toLeftOf="@+id/button_container"
                        app:layout_constraintStart_toEndOf="@+id/profile_picture"
                        app:layout_constraintTop_toTopOf="@+id/displayname" />


                    <androidx.constraintlayout.widget.ConstraintLayout
                        android:id="@+id/button_container"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintTop_toTopOf="parent">

                        <Button
                            android:id="@+id/logout_button"
                            style="@style/Widget.AppCompat.Button.Colored"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:singleLine="true"
                            android:text="@string/action_logout"
                            android:textAllCaps="false"
                            app:layout_constraintStart_toStartOf="parent"
                            app:layout_constraintTop_toTopOf="parent" />

                        <Button
                            android:id="@+id/settings"
                            style="@style/Widget.AppCompat.Button.Colored"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:singleLine="true"
                            android:text="@string/settings"
                            android:textAllCaps="false"
                            app:layout_constraintBottom_toBottomOf="parent"
                            app:layout_constraintEnd_toEndOf="parent"
                            app:layout_constraintStart_toStartOf="parent"
                            app:layout_constraintTop_toBottomOf="@+id/logout_button" />

                    </androidx.constraintlayout.widget.ConstraintLayout>
                </androidx.constraintlayout.widget.ConstraintLayout>
            </LinearLayout>
        </com.google.android.material.appbar.CollapsingToolbarLayout>

        <com.google.android.material.tabs.TabLayout
            android:id="@+id/account_tabLayout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:tabGravity="fill"
            app:tabMode="fixed"
            app:tabSelectedTextColor="?colorAccent"
            app:tabTextColor="@android:color/white" />
    </com.google.android.material.appbar.AppBarLayout>

    <TextView
        android:id="@+id/remote_account"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_gravity="center"
        android:layout_margin="20dp"
        android:autoLink="web"
        android:gravity="center"
        android:textSize="16sp"
        android:visibility="gone" />

    <androidx.viewpager.widget.ViewPager
        android:id="@+id/account_viewpager"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="5dp"
        app:layout_behavior="@string/appbar_scrolling_view_behavior" />

    <com.google.android.material.floatingactionbutton.FloatingActionButton
        android:id="@+id/action_button"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="bottom|end"
        android:layout_margin="@dimen/fab_margin_button"
        android:src="@drawable/ic_baseline_add_24"
        android:tint="@android:color/white"
        android:visibility="gone"
        tools:ignore="ContentDescription" />
</androidx.coordinatorlayout.widget.CoordinatorLayout>